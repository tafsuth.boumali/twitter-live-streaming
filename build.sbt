

name := "twitter-live-streaming"


val commonResolvers = Seq( "Maven Central" at "http://repo1.maven.org/maven2/",
  "Spray.io Releases" at "http://repo.spray.io/",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases",
  Resolver.bintrayRepo("iheartradio","maven")
)

//offline := true

val commonSettings = Seq (
  version := "0.1",
  scalaVersion := "2.11.8",
  envVars := Map("SPARK_LOCAL_IP" -> "localhost")
)

Defaults.itSettings

val baseConfigs: Seq[Configuration] = Seq(IntegrationTest)
lazy val integrationTestsSettings = inConfig(IntegrationTest)(Defaults.itSettings)

lazy val dataModel =Project(
  id = "data-model",
  base = file("data-model")
).settings(commonSettings: _*)

lazy val twitterLiveSpark =
  Project(
    id = "twitter-live-spark",
    base = file("twitter-live-spark"))
      .settings( resolvers := commonResolvers)
      .settings(Defaults.itSettings)
      .settings ( Seq(
      libraryDependencies ++= {
        val sparkVersion = "1.6.0"
        Seq(
          "com.typesafe" % "config" % "1.3.1",
          "ch.qos.logback" % "logback-classic" % "1.1.2",
          "org.scalatest" %% "scalatest" % "2.2.5" % "test,it",
          "org.apache.spark" %% "spark-core" % sparkVersion
            exclude ("org.slf4j", "slf4j-log4j12")
            exclude("org.apache.hadoop","hadoop-yarn-api")
            exclude("commons-beanutils", "commons-beanutils")
            exclude("commons-logging", "commons-logging")
            exclude("commons-collections", "commons-collections")
            exclude("com.esotericsoftware.minlog", "minlog")
            exclude("org.apache.avro","avro-ipc")
            exclude ("org.slf4j", "slf4j-api")
            exclude("org.apache.avro", "avro") ,
          "org.apache.spark" %% "spark-sql" % sparkVersion
            exclude ("org.slf4j", "slf4j-log4j12")
            exclude("commons-beanutils", "commons-beanutils")
            exclude("com.esotericsoftware.minlog", "minlog")
            exclude("org.apache.parquet", "parquet-format")
            exclude("org.apache.avro","avro-ipc")
            exclude ("org.slf4j", "slf4j-api")
            exclude("org.apache.avro", "avro") ,
          "org.apache.spark" %% "spark-streaming" % sparkVersion
            exclude("commons-beanutils", "commons-beanutils")
            exclude("com.esotericsoftware.minlog", "minlog")
            exclude("org.apache.avro","avro-ipc")
            exclude ("org.slf4j", "slf4j-api")
            exclude ("org.slf4j", "slf4j-log4j12")
            exclude("org.apache.avro", "avro") ,
          "com.iheart" %% "ficus" % "1.1.3" exclude("com.chuusai", "shapeless") exclude("com.typesafe", "config"),
          "org.apache.spark" %% "spark-streaming-twitter" % sparkVersion
            exclude ("org.slf4j", "slf4j-log4j12")
            exclude("commons-beanutils", "commons-beanutils")
            exclude("com.esotericsoftware.minlog", "minlog")
            exclude("org.apache.avro","avro-ipc")
            exclude ("org.slf4j", "slf4j-api")
            exclude("org.apache.avro", "avro") ,
          "com.datastax.spark" %% "spark-cassandra-connector" % sparkVersion
            exclude ("org.slf4j", "slf4j-log4j12")
            exclude("commons-beanutils", "commons-beanutils")
            exclude("io.netty", "netty-common")
            exclude("io.netty", "netty-codec")
            exclude("io.netty", "netty-transport")
            exclude("io.netty", "netty-buffer")
            exclude("io.netty", "netty-handler")
            exclude("org.apache.avro","avro-ipc")
            exclude ("org.slf4j", "slf4j-api")
            exclude("org.apache.avro", "avro") ,
          "net.databinder.dispatch" %% "dispatch-core" % "0.11.2",
          "com.holdenkarau" %% "spark-testing-base" % "1.6.1_0.3.3"
        )
      }.map(_.excludeAll(
      //  ExclusionRule("log4j", "log4j"),
      //  ExclusionRule("org.slf4j", "slf4j-api"),
        ExclusionRule("jline", "jline")
      )),
      mainClass in (Compile, packageBin) := Some("com.zengu.twitter.live.TweetStreamDriver")
    )
  ).settings(
    sbtassembly.AssemblyKeys.assemblyMergeStrategy in sbtassembly.AssemblyKeys.assembly := {
      case "META-INF/io.netty.versions.properties" => {
        sbtassembly.AssemblyPlugin.autoImport.MergeStrategy.discard
      }
      case PathList(xs @ _*) if xs.last == "UnusedStubClass.class" => MergeStrategy.first
      case x =>
        val oldStrategy = (sbtassembly.AssemblyKeys.assemblyMergeStrategy in sbtassembly.AssemblyKeys.assembly).value
        oldStrategy(x)
    }
  )
    .settings(commonSettings: _*)
    .configs(baseConfigs: _*)
    .dependsOn(dataModel)


imageNames in docker := Seq(
  ImageName(
    repository = name.value,
    tag = Some(sys.props.getOrElse("IMAGE_TAG", default = version.value))
  )
)

val twitterLiveApi =    Project(
  id = "twitter-live-api",
  base = file("twitter-live-api"))
  .settings( resolvers := commonResolvers)
  .settings(Defaults.itSettings)
  .enablePlugins(sbtdocker.DockerPlugin)
  .settings(

    mainClass in (Compile, run) := Some("com.zengu.twitter.api.TweeterApiBoot"),

    dockerfile in docker := {
      val baseDir = baseDirectory.value
      val artifact: File = assembly.value
      val imageAppBaseDir = "/app"
      val artifactTargetPath = s"$imageAppBaseDir/${artifact.name}"
      val artifactTargetPath_ln = s"$imageAppBaseDir/${name.value}.jar"
      //Define the resources which includes the entrypoint script
      val dockerResourcesDir = baseDir / "docker-resources"
      val dockerResourcesTargetPath = s"$imageAppBaseDir/"
      val appConfTarget = s"$imageAppBaseDir/conf/application"
      val logConfTarget = s"$imageAppBaseDir/conf/logging" //logback.xml
      new Dockerfile {
        from("openjdk:8-jre")
        expose(3949, 3949)
        env("APP_BASE", s"$imageAppBaseDir")
        env("APP_CONF", s"$appConfTarget")
        env("LOG_CONF", s"$logConfTarget")
        copy(artifact, artifactTargetPath)
        copy(dockerResourcesDir, dockerResourcesTargetPath)
        copy(baseDir / "src" / "main" / "resources" / "logback.xml", logConfTarget) //Copy the default logback.xml
        //Symlink the service jar to a non version specific name
        run("ln", "-sf", s"$artifactTargetPath", s"$artifactTargetPath_ln")
        entryPoint(s"${dockerResourcesTargetPath}docker-entrypoint.sh")
      }
    }
  )
  .settings ( Seq(
    libraryDependencies ++= {
      val sparkVersion = "1.6.0"
      val akkaVersion       = "2.3.9"
      val sprayVersion      = "1.3.3"
      Seq(
        "com.typesafe" % "config" % "1.3.1",
        "ch.qos.logback" % "logback-classic" % "1.1.2",
        "org.scalatest" %% "scalatest" % "2.2.5" % "test,it",
        "org.apache.spark" %% "spark-core" % sparkVersion
          exclude ("org.slf4j", "slf4j-log4j12")
          exclude("org.apache.hadoop","hadoop-yarn-api")
          exclude("commons-beanutils", "commons-beanutils")
          exclude("commons-logging", "commons-logging")
          exclude("commons-collections", "commons-collections")
          exclude("com.esotericsoftware.minlog", "minlog")
          exclude ("org.slf4j", "slf4j-api")
          exclude("org.apache.avro", "avro")
          exclude("org.apache.avro","avro-ipc") ,
        "org.apache.spark" %% "spark-sql" % sparkVersion
          exclude ("org.slf4j", "slf4j-log4j12")
          exclude("commons-beanutils", "commons-beanutils")
          exclude("com.esotericsoftware.minlog", "minlog")
          exclude("org.apache.parquet", "parquet-format")
          exclude ("org.slf4j", "slf4j-api")
          exclude("org.apache.avro", "avro")
          exclude("org.apache.avro","avro-ipc") ,
        "com.iheart" %% "ficus" % "1.1.3"
          exclude("com.chuusai", "shapeless")
          exclude("com.typesafe", "config"),
        "com.typesafe.akka" %% "akka-actor"      % akkaVersion,
        "io.spray"          %% "spray-can"       % sprayVersion,
        "io.spray"          %% "spray-routing"   % sprayVersion,
        "io.spray"          %% "spray-client"   % sprayVersion % "test,it",
        "io.spray"          %% "spray-json"      % sprayVersion,
        "com.typesafe.akka" %% "akka-testkit"    % akkaVersion  % "test",
        "io.spray"          %% "spray-testkit"   % sprayVersion % "test",
        "com.datastax.spark" %% "spark-cassandra-connector" % "1.6.0"
          exclude ("org.slf4j", "slf4j-log4j12")
          exclude("commons-beanutils", "commons-beanutils")
          exclude("io.netty", "netty-common")
          exclude("io.netty", "netty-codec")
          exclude("io.netty", "netty-transport")
          exclude("io.netty", "netty-buffer")
          exclude("io.netty", "netty-handler")
          exclude("org.apache.avro","avro-ipc")
         // exclude ("org.slf4j", "slf4j-api")
          exclude("org.apache.avro", "avro") ,
        "org.mockito" % "mockito-all" % "1.8.4"  % "test"
      )
    }.map(_.excludeAll(
      ExclusionRule("jline", "jline")
    )),
    mainClass in (Compile, packageBin) := Some("com.zengu.twitter.api.TweeterApiBoot")
  )
  )
  .settings(
    sbtassembly.AssemblyKeys.assemblyMergeStrategy in sbtassembly.AssemblyKeys.assembly := {
      case "META-INF/io.netty.versions.properties" => {
        sbtassembly.AssemblyPlugin.autoImport.MergeStrategy.discard
      }
      case PathList(xs @ _*) if xs.last == "UnusedStubClass.class" => MergeStrategy.first
      case x =>
        val oldStrategy = (sbtassembly.AssemblyKeys.assemblyMergeStrategy in sbtassembly.AssemblyKeys.assembly).value
        oldStrategy(x)
    }
  )
  .settings(commonSettings: _*)
  .configs(baseConfigs: _*)
  .dependsOn(dataModel)





lazy val root = project.in(file(".")).configs(IntegrationTest).aggregate(dataModel, twitterLiveSpark, twitterLiveApi)


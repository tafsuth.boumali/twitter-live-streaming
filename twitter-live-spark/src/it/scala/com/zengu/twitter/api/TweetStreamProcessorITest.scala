package com.zengu.twitter.api

import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import scala.util.Random

class TweetStreamProcessorITest extends FlatSpec
  with Matchers
  with BeforeAndAfterAll{

  val schemaManager = new CassandraSupport(ConfigFactory.load())

  val conf = new TweetStreamConfiguration(ConfigFactory.load())

  val sc = new SparkContext(conf.sparkConf.setMaster(s"local[5]")
    .setAppName(s"${getClass.getSimpleName}-${Random.nextLong()}"))

  lazy val streamingContext = new StreamingContext(sc, conf.batchWindow)

  val processor = new TweetStreamProcessor (streamingContext,conf)

  lazy val session = schemaManager.cluster.connect(conf.Model.Keyspace)

  it should "insert words and links into cassandra from real tweets" in {
    processor.run()

    streamingContext.start()

    import org.scalatest.concurrent.Eventually._

    import scala.concurrent.duration._
    eventually (timeout(120 seconds), interval(500 millis)){
      import com.datastax.spark.connector._
      assert(streamingContext.sparkContext.cassandraTable(conf.Model.Keyspace, conf.Model.WordsTable).cassandraCount > 1)
      assert(streamingContext.sparkContext.cassandraTable(conf.Model.Keyspace, conf.Model.LinksTable).cassandraCount > 1)
    }


  }

  override protected def beforeAll(): Unit = {

    assume(schemaManager.executeScriptFromFile("twitter.cql"), "Cassandra schema not created")
    session.execute(s"TRUNCATE ${conf.Model.Keyspace}.${conf.Model.LinksTable}")
    session.execute(s"TRUNCATE ${conf.Model.Keyspace}.${conf.Model.WordsTable}")

  }

  override protected def afterAll(): Unit = {
    streamingContext.stop()
  }

}

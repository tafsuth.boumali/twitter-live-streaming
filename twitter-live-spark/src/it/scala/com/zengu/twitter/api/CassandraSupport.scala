package com.zengu.twitter.api

import java.io.Closeable
import java.net.InetSocketAddress

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.exceptions.SyntaxError
import com.typesafe.config.Config
import org.slf4j.LoggerFactory

import scala.io.Source

class CassandraSupport(config : Config) extends Closeable {

  def executeScriptFromFile(filePath: String): Boolean = executeScript(Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(filePath)))

  val logger = LoggerFactory.getLogger(classOf[CassandraSupport])

  import scala.collection.JavaConversions._

  val CASSANDRA_SEEDS_CONF_KEY = "tweets.cassandra.seeds"

  lazy val seeds = for (seed ← config.getConfigList(CASSANDRA_SEEDS_CONF_KEY)) yield new InetSocketAddress(seed.getString("host"), seed.getInt("port"))

  lazy val cluster = Cluster.builder()
    .withoutJMXReporting()
    .withoutMetrics()
    .addContactPointsWithPorts(seeds)
    .build()

  def executeScript(source: Source): Boolean = {
    val session = cluster.newSession()

    def execute(statement: String) = {
      try {
        session.execute(statement)
      } catch {
        case (se: SyntaxError) ⇒ {
          logger.error(s"fail to execute statement ${statement} because of a SyntaxError ${se.getStackTraceString}")
          throw se
        }
      }
    }

    try {
      val statements = buildStatements(source)
      statements.foldLeft(true)((result, statement) ⇒ result && execute(statement).wasApplied())
    } finally {
      session.close()
    }

  }

  def buildStatements(source: Source) = {
    val script = source.getLines().mkString
    script.split(";").filter(s ⇒ !s.isEmpty && !s.trim.isEmpty)
  }

  override def close(): Unit = cluster.close()
}

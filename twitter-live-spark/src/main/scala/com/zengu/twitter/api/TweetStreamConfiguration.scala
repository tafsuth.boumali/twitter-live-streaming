package com.zengu.twitter.api

import com.typesafe.config.Config
import org.apache.spark.SparkConf
import org.apache.spark.streaming.Milliseconds

import scala.concurrent.duration.{Duration, FiniteDuration}


class TweetStreamConfiguration (config : Config) extends Serializable  {

  import net.ceedubs.ficus.Ficus._

  val CONFIG_PREFIX = "tweets"

  val max = 10

  val httpRequestDuration = Duration("1 s")

  object Model extends Serializable {
    val Keyspace = "twitter"
    val LinksTable = "links"
    val WordsTable = "words"
  }

  lazy protected val internalSettings = config.getConfig(CONFIG_PREFIX)

  @transient val sparkConf = new SparkConf()
      .set("spark.cassandra.connection.host",internalSettings.getString("sparkConf.cassandra.host") )
      .set("spark.cassandra.connection.port",internalSettings.getString("sparkConf.cassandra.port") )

  lazy val batchWindow = Milliseconds(internalSettings.as[FiniteDuration]("batchWindow").toMillis)
}

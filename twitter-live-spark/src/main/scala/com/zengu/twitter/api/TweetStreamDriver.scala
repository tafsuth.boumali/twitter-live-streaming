package com.zengu.twitter.api

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext

class TweetStreamDriver (config : Config) {

  val configuration =  new TweetStreamConfiguration (config)

  def run(): Unit = {
    val sc = new SparkContext(configuration.sparkConf)

    val ssc = new StreamingContext(sc, configuration.batchWindow)

    try {
      new TweetStreamProcessor(ssc,  configuration).run()
      ssc.start()
    } finally {
      ssc.awaitTermination()
      ssc.stop(true, true)
    }

  }
}

object TweetStreamDriver extends App {
  val driver = new TweetStreamDriver(ConfigFactory.load)
  driver.run()
}

package com.zengu.twitter.api

import com.datastax.spark.connector.SomeColumns
import com.datastax.spark.connector.streaming._
import dispatch.Defaults._
import dispatch._
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.twitter.TwitterUtils
import twitter4j.Status

import scala.concurrent.duration.Duration

class TweetStreamProcessor(ssc: StreamingContext, configuration: TweetStreamConfiguration) extends Serializable {

  import TweetStreamProcessor._
  def run(): Unit = {

    System.setProperty("twitter4j.oauth.consumerKey", "YY")
    System.setProperty("twitter4j.oauth.consumerSecret", "YY")
    System.setProperty("twitter4j.oauth.accessToken", "519725320-ZZ")
    System.setProperty("twitter4j.oauth.accessTokenSecret", "CC")


    val stream: DStream[Status] = TwitterUtils.createStream(ssc, None).transform( rdd => rdd.sample(false, 0.1))

    val transformedStream : DStream[(Boolean,Status)] = transform(stream)

    //transformedStream.cache()

    val tweetsWithLinks = transformedStream.filter { case (hasUrl, tweet) => hasUrl }.map { case (key, value) => value}
    val linksCount = countLinks(tweetsWithLinks, configuration.httpRequestDuration )
    linksCount.saveToCassandra(configuration.Model.Keyspace, configuration.Model.LinksTable, SomeColumns("lnk", "cnt"))


    val tweetsWithoutLinks = transformedStream.filter { case (hasUrl, tweet) => !hasUrl }.map { case (key, value) => value }
    val wordsCount = countWords(tweetsWithoutLinks)
    wordsCount.saveToCassandra(configuration.Model.Keyspace, configuration.Model.WordsTable, SomeColumns("wrd", "cnt"))

  }


}

object TweetStreamProcessor {


  def transform (stream: DStream[Status]) : DStream[(Boolean, Status)] = {
    stream.map(tweet => (!tweet.getURLEntities.isEmpty, tweet))
  }

  def countWords ( stream : DStream[Status]) : DStream[(String, Int)]= {
    stream.flatMap(tweet => tweet.getText.split(" "))
      .filter(word => ! word.isEmpty)
        .filter(word => word.matches("[A-Za-z]+") && word.length > 2)
      .map(word  => (word, 1))
      .reduceByKey(_ + _)
  }



  def countLinks  ( stream : DStream[Status], duration : Duration) : DStream[(String, Int)] = {
    stream.flatMap { tweet : Status =>
      tweet.getURLEntities.map(url => URIFollower.unshorttenUri(url.getURL(), duration)
      )
    }
     .filter( link => link.isDefined)
     .map(link => (link.get, 1))
     .reduceByKey(_ + _)
  }

}

object URIFollower {

  def unshorttenUri(uri: String, duration: Duration): Option[String] = {
    import scala.concurrent.Await

    lazy val finalUrl = Http.configure(_ setFollowRedirects true)(url(uri) OK as.Response(_.getUri))
    try {
      Some(Await.result(finalUrl, duration).toString)
    } catch {
      case _: Exception => None
    }
  }

}



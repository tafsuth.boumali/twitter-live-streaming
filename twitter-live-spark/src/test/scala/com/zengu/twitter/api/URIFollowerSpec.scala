package com.zengu.twitter.api

import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._


class URIFollowerSpec extends FlatSpec with Matchers{

 "unshorttenUri" should "unshort good uri" in {
   URIFollower.unshorttenUri("http://bit.ly/2kYPRnx", Duration("1 s")) should be (Some("http://www.journaldugeek.com/2017/02/13/the-batman-le-realisateur-de-la-planete-des-singes-pressenti-pour-remplacer-ben-affleck/"))
 }

  it should "return None on bad URL" in {
    URIFollower.unshorttenUri("https://goo.gle/Plnzty123", 10.seconds) should be (None)
  }

  it should "return same url if it is already expanded" in {
    val url = "https://github.com/"
    URIFollower.unshorttenUri(url, 10.seconds) should be (Some(url))
  }

}

package com.zengu.twitter.api

import com.holdenkarau.spark.testing.StreamingSuiteBase
import org.apache.spark.streaming.dstream.DStream
import org.scalatest.FunSuite
import twitter4j.{Status, TwitterObjectFactory}

import scala.concurrent.duration._
class TweetStreamProcessorSpec extends FunSuite with StreamingSuiteBase{

  import Fixture._


  test("count links in tweets") {
    val input = List(List( tweetLinkFollowRedirect), List(tweetLinkFalseURL))
    val expected = List( List((finalUrl,1)) , List() )
    testOperation[Status, (String,Int)]( input, TweetStreamProcessor.countLinks(_ : DStream[Status], 1.seconds)  , expected, ordered = false)
  }


  test("count words in tweets") {
    val input = List(List( tweetWord1), List(tweetWord2))
    val expected = List( List(("first",1), ("tweet",1)) , List(("second",1), ("tweet",3), ("word", 2)))
    testOperation[Status, (String,Int)]( input, TweetStreamProcessor.countWords  _  , expected, ordered = false)
  }

  
  test("transform") {
    val input = List(List( tweetLinkFollowRedirect), List(tweetWord1))
    val expected = List( List((true, tweetLinkFollowRedirect)) , List((false,tweetWord1)) )
    testOperation[Status, (Boolean, Status)]( input, TweetStreamProcessor.transform(_ : DStream[Status])  , expected, ordered = false)
  }

}

object TweetSerDe {

  def fromString (json : String) = {
      TwitterObjectFactory.createStatus(json)
  }

}


object Fixture {

  val tweetWord1 = TweetSerDe.fromString("""{text="first tweet"}""")
  val tweetWord2 = TweetSerDe.fromString("""{text="second tweet tweet tweet word word"}""")

  val finalUrl = "http://www.journaldugeek.com/2017/02/13/the-batman-le-realisateur-de-la-planete-des-singes-pressenti-pour-remplacer-ben-affleck/"

  val tweetLinkFollowRedirect = TweetSerDe.fromString(
    """{text:"whatever",entities:{urls:[{url:"http://bit.ly/2kYPRnx",expanded_url:"https://twitter.com/i/web/status/714461850188926976",display_url:"twitter.com/i/web/status/7…",indices:[117,140]}]}}""")


  val tweetLinkFalseURL= TweetSerDe.fromString("""{text="whatever",entities:{urls:[{url:"https://goo.gle/Plnzty123",expanded_url:"https://twitter.com/i/web/status/714461850188926976",display_url:"twitter.com/i/web/status/7…",indices:[117,140]}]}}""")



}
#!/bin/bash
set -e
set -x
java -Dlogger.url=file:///${LOG_CONF}/logback.xml -cp ${APP_BASE}/conf -jar ${APP_BASE}/twitter-live-api.jar
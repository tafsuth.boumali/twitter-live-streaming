package com.zengu.twitter.api.routes

import com.typesafe.config.ConfigFactory
import com.zengu.twitter.api.ApiConfiguration
import com.zengu.twitter.api.view.{PopularInTweet, Top}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import org.mockito.BDDMockito._
import com.zengu.twitter.api.SchemaInformation._
import spray.http.StatusCodes

class PopularWordsRouteSpec extends FlatSpec
  with ScalatestRouteTest
  with PopularWordsRoute
  with MockitoSugar
  with Matchers
  with HttpService {

  val apiConfiguration = new ApiConfiguration(ConfigFactory.load())
  override implicit def actorRefFactory = system

  override lazy val viewer  = mock[PopularInTweet]
  import com.zengu.twitter.api.routes.PopularTweetsProtocol._

  it must "return top words " in {
    given(viewer.topResults(Keyspace, WordsTable, WordColumn, CountColumn, 2)).willReturn(Array(Top("word1",100), Top("word2", 99)))
    Get("/words/top/2") ~> popularWordsRoute ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[Array[Top]]
        assert(response === Array(Top("word1",100), Top("word2",99)))
    }
  }


  }


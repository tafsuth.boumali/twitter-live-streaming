package com.zengu.twitter.api.routes


import com.typesafe.config.ConfigFactory
import com.zengu.twitter.api.ApiConfiguration
import com.zengu.twitter.api.view.{PopularInTweet, Top}
import org.scalatest.mock.MockitoSugar
import org.mockito.BDDMockito._
import org.scalatest.{FlatSpec, Matchers}
import spray.http.StatusCodes
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import com.zengu.twitter.api.SchemaInformation._

class PopularLinksRouteSpec extends FlatSpec
  with ScalatestRouteTest
  with PopularLinksRoute
  with MockitoSugar
  with Matchers
  with HttpService {

  val apiConfiguration = new ApiConfiguration(ConfigFactory.load())

  override implicit def actorRefFactory = system

  import com.zengu.twitter.api.routes.PopularTweetsProtocol._

  import PopularTweetsProtocol._
  override lazy val viewer  = mock[PopularInTweet]

  it must "return top links " in {
    given(viewer.topResults(Keyspace, LinksTable, LinkColumn, CountColumn, 2)).willReturn(Array(Top("link1",100), Top("http://2", 99)))

    // given(viewer.topResults(Keyspace, LinksTable, LinkColumn, CountColumn, 2)).willReturn(Future(Array(Top("link1",100), Top("http://2", 99))))
    Get("/links/top/2") ~> popularLinksRoute ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[Array[Top]]
      assert(response === Array(Top("link1",100), Top("http://2",99)))
    }
  }


  }


package com.zengu.twitter.api.routes

import com.typesafe.config.ConfigFactory
import com.zengu.twitter.api.ApiConfiguration
import com.zengu.twitter.api.view.{PopularInTweet, Top}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import org.mockito.BDDMockito._
import com.zengu.twitter.api.SchemaInformation._
import spray.http.StatusCodes

class RoutesSpec extends FlatSpec
  with ScalatestRouteTest
  with Routes
  with MockitoSugar
  with Matchers
  with HttpService {

  val apiConfiguration = new ApiConfiguration(ConfigFactory.load())

  import com.zengu.twitter.api.routes.PopularTweetsProtocol._

  override implicit def actorRefFactory = system
  override lazy val viewer  = mock[PopularInTweet]

  it must "return top links using full path" in {
    given(viewer.topResults(Keyspace, LinksTable, LinkColumn, CountColumn, 2)).willReturn(Array(Top("link1",100), Top("http://2", 99)))
    Get("/tweets/api/v1.0/popular/links/top/2") ~> routes ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[Array[Top]]
      assert(response === Array(Top("link1",100), Top("http://2",99)))
    }
  }


  }


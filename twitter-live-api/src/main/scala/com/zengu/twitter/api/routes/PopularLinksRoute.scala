package com.zengu.twitter.api.routes

import com.zengu.twitter.api.WithApiConfiguration
import spray.routing.{HttpService, Route}

trait PopularLinksRoute  extends WithApiConfiguration with HttpService with TweetViewer {

  import com.zengu.twitter.api.SchemaInformation._

  def popularLinksRoute: Route = {
    pathPrefix("links" / "top" / IntNumber) { maxWords =>
      get {
        import com.zengu.twitter.api.routes.PopularTweetsProtocol._
        complete(viewer.topResults(Keyspace, LinksTable, LinkColumn, CountColumn, maxWords))
      /*  onComplete(viewer.topResults(Keyspace, LinksTable, LinkColumn, CountColumn, maxWords)) {
          case Success(value) => complete(value)
          case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
        }*/
      }
    }
  }
}


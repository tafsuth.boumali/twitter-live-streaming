package com.zengu.twitter.api

import com.typesafe.config.Config
import org.apache.spark.SparkConf

class ApiConfiguration(config : Config ) {

  private val confPrefix = "tweets.api"
  require(config.hasPath(confPrefix), s"configuration should define ${confPrefix} namespace")
  private val internalSettings = config.getConfig(confPrefix)

  @transient val sparkConf = new SparkConf()
    .set("spark.cassandra.connection.host",internalSettings.getString("sparkConf.cassandra.host") )
    .set("spark.cassandra.connection.port",internalSettings.getString("sparkConf.cassandra.port") )
      .setAppName("app")
    .setMaster("local[3]")


}


object SchemaInformation {

  val Keyspace = "twitter"

  val WordsTable = "words"
  val WordColumn = "wrd"

  val LinkColumn = "lnk"
  val LinksTable = "links"

  val CountColumn = "cnt"
}

trait WithApiConfiguration {
  val apiConfiguration: ApiConfiguration
}
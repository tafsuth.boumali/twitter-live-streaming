package com.zengu.twitter.api

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory
import spray.can.Http
import akka.io.Tcp.{Bound, CommandFailed}
import spray.routing.HttpServiceActor
import akka.pattern.ask
import com.zengu.twitter.api.routes.Routes


object TweeterApiBoot extends App with Server {

  override implicit val system: ActorSystem = ActorSystem("campaign")
  start(ConfigFactory.load())

}

trait Server {

  private val logger = LoggerFactory.getLogger(classOf[Server])

  implicit val system: ActorSystem

  def start(configuration: Config): Unit = {

    val actor = system.actorOf(Props(classOf[TweeterApi], configuration), "Api")
    val serverSettings = new ServerConfiguration(configuration)
    val networkInterface = serverSettings.listeningInterface
    val port = serverSettings.listeningPort
    val bounded = IO(Http)(system).ask(Http.Bind(actor, interface = networkInterface, port = port))(serverSettings.bindingTimeout)

    implicit val ctx = system.dispatcher

    bounded onSuccess {
      case _: Bound ⇒ {
        logger.info("server successfully bound to {}:{}", networkInterface, port)
      }
      case _ : CommandFailed⇒ {
        logger.error("server failed to bound to {}:{}", networkInterface, port)
        system.shutdown()
      }
    }

    bounded onFailure {
      case t: RuntimeException ⇒ {
        logger.error(s"binding attempt failed ${t.getMessage}", t)
        system.shutdown()
      }
      case e: Throwable ⇒ {
        logger.error("binding attempt failed with unknown reason!" + e.getMessage, e)
        system.shutdown()
      }
    }

  }
}


class TweeterApi(baseConfig : Config) extends HttpServiceActor with Routes {

  override val apiConfiguration: ApiConfiguration = new ApiConfiguration(baseConfig)
  def receive = runRoute(routes)

}
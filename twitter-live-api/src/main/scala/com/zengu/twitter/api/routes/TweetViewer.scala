package com.zengu.twitter.api.routes

import com.zengu.twitter.api.WithApiConfiguration
import com.zengu.twitter.api.view.PopularInTweet
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext


trait TweetViewer {
  this : WithApiConfiguration =>

  lazy val viewer = new PopularInTweet(new CassandraSQLContext(new SparkContext(apiConfiguration.sparkConf)))
}

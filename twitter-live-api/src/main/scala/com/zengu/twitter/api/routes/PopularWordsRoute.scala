package com.zengu.twitter.api.routes

import com.zengu.twitter.api.SchemaInformation._
import com.zengu.twitter.api.WithApiConfiguration
import spray.routing.{HttpService, Route}

trait PopularWordsRoute extends HttpService with WithApiConfiguration with TweetViewer {


  def popularWordsRoute: Route = {
    pathPrefix("words" / "top" / IntNumber) { maxWords =>
      get {
          import com.zengu.twitter.api.routes.PopularTweetsProtocol._

        complete(viewer.topResults(Keyspace, WordsTable, WordColumn, CountColumn, maxWords))

         /* onComplete(viewer.topResults(Keyspace, WordsTable, WordColumn, CountColumn, maxWords)) {
            case Success(value) => complete(value)
            case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
          }*/
        }
      }
    }
}

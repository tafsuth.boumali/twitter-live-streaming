package com.zengu.twitter.api.routes

import spray.http.StatusCodes
import spray.routing.ExceptionHandler


trait Routes extends PopularLinksRoute with PopularWordsRoute{

  val exceptionRequestHandler = ExceptionHandler {
    //TODO handle ex
    case exception @ (_: RuntimeException) ⇒ complete((StatusCodes.InternalServerError, exception.getMessage))
  }


  def routes = handleExceptions(exceptionRequestHandler) {
    pathPrefix ("tweets" / "api" / Versions.`V1.0` / "popular" ) {
      popularWordsRoute ~  popularLinksRoute
    }
  }
}


object Versions {

  val `V1.0` = "v1.0"

}



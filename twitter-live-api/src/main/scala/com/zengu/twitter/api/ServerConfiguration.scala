package com.zengu.twitter.api

/**
  * Created by tafsuthboumali on 15/02/2017.
  */
import com.typesafe.config.Config

class ServerConfiguration(config: Config) {
  private val confPrefix = "tweets.server"

  require(config.hasPath(confPrefix), s"configuration should define ${confPrefix} namespace")

  private val settings = config.getConfig(confPrefix)
  val listeningInterface = settings.getString("listeningInterface")
  val listeningPort = settings.getInt("listeningPort")
  import net.ceedubs.ficus.Ficus._

  import scala.concurrent.duration.FiniteDuration
  val bindingTimeout = settings.as[FiniteDuration]("bindingTimeout")

 //quickSort
  def sort(tab:Array[Int]): Array[Int] =
    if (tab.length < 2) tab
    else {
      val pivot = tab (tab.length / 2)
      sort (tab filter (pivot>)) ++ (tab filter (pivot == )) ++ sort (tab filter(pivot <))
    }

}
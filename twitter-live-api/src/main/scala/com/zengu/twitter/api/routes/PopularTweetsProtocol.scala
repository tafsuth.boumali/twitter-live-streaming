package com.zengu.twitter.api.routes

import com.zengu.twitter.api.view.Top
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait PopularTweetsProtocol extends DefaultJsonProtocol with SprayJsonSupport {

  implicit def topFormat = jsonFormat2(Top)

}


object PopularTweetsProtocol extends PopularTweetsProtocol
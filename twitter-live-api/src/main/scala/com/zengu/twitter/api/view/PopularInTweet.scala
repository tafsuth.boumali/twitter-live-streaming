package com.zengu.twitter.api.view

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.cassandra.CassandraSQLContext

class PopularInTweet (sqlC: CassandraSQLContext) {

  def topResults(keyspace: String, table: String, column: String, counter: String, maxResults : Int): Array[Top] = {

    // enlever future a cause de spark.sql.execution.id already set
   // Future{
    sqlC.setKeyspace(keyspace)

    val words: DataFrame = sqlC.cassandraSql(s"SELECT ${column} , ${counter} " +
      s"FROM ${table} ")

    import org.apache.spark.sql.functions._
     words.orderBy(desc(counter)).take(maxResults).map(row => Top(row.getString(0), row.getLong(1)))
   // }



  }


}

case class Top (popular : String , count : Long)


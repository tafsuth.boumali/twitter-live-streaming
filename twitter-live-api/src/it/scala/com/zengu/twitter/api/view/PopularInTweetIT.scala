package com.zengu.twitter.api.view

import com.datastax.driver.core.{BoundStatement, Session, SimpleStatement}
import com.typesafe.config.ConfigFactory
import com.zengu.twitter.api.ApiConfiguration
import com.zengu.twitter.api.SchemaInformation._
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

class PopularInTweetIT extends FlatSpec with Matchers with BeforeAndAfterAll{

  val schemaManager = new CassandraSupport(ConfigFactory.load())

  val conf = new ApiConfiguration(ConfigFactory.load())

  val sconf = conf.sparkConf.setMaster("local[5]").setAppName("PopularInTweet")
  lazy val session  = schemaManager.cluster.connect(Keyspace)

  val sqlC = new CassandraSQLContext(new SparkContext(sconf))

  override protected def beforeAll(): Unit = {

    assume(schemaManager.executeScriptFromFile("twitter.cql"), "Cassandra schema not created")
    session.execute(s"TRUNCATE ${Keyspace}.${LinksTable}")
    session.execute(s"TRUNCATE ${Keyspace}.${WordsTable}")

    //insert words
    Support.insert(session, WordsTable, WordColumn, "word1", 10 )
    Support.insert(session, WordsTable, WordColumn, "word2", 1 )
    Support.insert(session, WordsTable, WordColumn, "word3", 50 )
    Support.insert(session, WordsTable, WordColumn, "word4", 70 )
    Support.insert(session, WordsTable, WordColumn, "word5", 80 )
    Support.insert(session, WordsTable, WordColumn, "word6", 10 )

    // insert links

    Support.insert(session, LinksTable, LinkColumn, "http://link1", 1 )
    Support.insert(session, LinksTable, LinkColumn, "http://link2", 2 )
    Support.insert(session, LinksTable, LinkColumn, "http://link3", 3 )
    Support.insert(session, LinksTable, LinkColumn, "http://link4", 4 )
    Support.insert(session, LinksTable, LinkColumn, "http://link5", 5 )
    Support.insert(session, LinksTable, LinkColumn, "http://link6", 6 )

  }

  "topResults " should "return the 3 most popular words" in {
    val viewer = new PopularInTweet(sqlC)
   // val tops = Await.result(viewer.topResults(Keyspace,WordsTable,WordColumn,CountColumn, 3),10.seconds)

    val tops = viewer.topResults(Keyspace,WordsTable,WordColumn,CountColumn, 3)
    tops should be (Array(Top("word5",80), Top("word4", 70), Top("word3", 50)))
  }

  it should "return the 2 most popular links" in {
    val viewer = new PopularInTweet(sqlC)
   // val tops = Await.result(viewer.topResults(Keyspace,LinksTable,LinkColumn,CountColumn, 2),10.seconds)

    val tops =viewer.topResults(Keyspace,LinksTable,LinkColumn,CountColumn, 2)
    tops should be (Array(Top("http://link6",6), Top("http://link5", 5)))
  }

}

object Support {

  def insert (session : Session, table : String, column : String, valueColumn : String, valueCnt : Int) = {

    val statement = s"update $table set cnt = cnt + $valueCnt " +
      s"where $column='$valueColumn'"

    val preparedStatement = session.prepare(new SimpleStatement(statement))

    session.execute(new BoundStatement(preparedStatement))

  }
}


